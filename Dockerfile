FROM python:3.8

ARG GEO_IP_URL
ARG GEO_IP_VERSION
ARG GEO_IP_PACKAGE
ARG GEO_IP_ACCOUNT_ID
ARG GEO_IP_LICENSE_KEY
ARG GEO_IP_EDITION_IDS
ARG GEO_IP_DATA_PATH
ARG GEO_IP_CONFIG_FILE

RUN apt-get update && apt-get install -y cmake bison flex gettext

ENV PYTHONPATH=/usr/src/app/
ENV PATH="$PATH:/root/.local/bin"

RUN pip3 install pip --upgrade
RUN pip install --user pipenv

WORKDIR /usr/src/app

COPY Pipfile /usr/src/app/

RUN pipenv install --skip-lock

COPY GeoIPTemplate.conf /usr/src/app/

ADD GeoIPTemplate.conf /home/app/GeoIPTemplate.conf

RUN echo $GEO_IP_URL/$GEO_IP_VERSION/$GEO_IP_PACKAGE
RUN wget $GEO_IP_URL/$GEO_IP_VERSION/$GEO_IP_PACKAGE
RUN dpkg -i ./$GEO_IP_PACKAGE

RUN envsubst < GeoIPTemplate.conf > $GEO_IP_CONFIG_FILE
RUN geoipupdate -v -f $GEO_IP_CONFIG_FILE

COPY . /usr/src/app/

EXPOSE 8080

COPY entrypoint.sh /usr/local/bin/
RUN chmod 777 /usr/local/bin/entrypoint.sh
RUN ln -s /usr/local/bin/entrypoint.sh /

ENTRYPOINT ["entrypoint.sh"]
