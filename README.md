# VTB

## Build and Run
```bash
docker-compose up --build
```

## ENV
```bash
export DATABASE_DRIVER=postgres
export DATABASE_USER=postgres
export DATABASE_PASSWORD=postgres
export DATABASE_HOST=db
export DATABASE_PORT=5435
export DATABASE_NAME=vtb
export DEBUG=true
export GEO_IP_DB_NAME="GeoLite2-City.mmdb"
export GEO_IP_URL="https://github.com/maxmind/geoipupdate/releases/export download"
export GEO_IP_VERSION="v4.3.0"
export GEO_IP_PACKAGE="geoipupdate_4.3.0_linux_amd64.deb"
export GEO_IP_ACCOUNT_ID=<account_id>
export GEO_IP_LICENSE_KEY=<licanse_key>
export GEO_IP_EDITION_IDS="GeoLite2-ASN GeoLite2-City export GeoLite2-Country"
export GEO_IP_DATA_PATH="/usr/share/GeoIP"
export GEO_IP_CONFIG_FILE="/etc/GeoIP.conf"
export TWILIO_ACCOUNT_SID=<sid>
export TWILIO_AUTH_TOKEN=<token>
export TWILIO_FROM_PHONE=<phone>
export HOST=0.0.0.0
export PORT=80
```