import os

from aiohttp import web
from tartiflette_aiohttp import register_graphql_handlers

from bank import config
from bank.startups import init_db
from bank.cleanups import close_db
from bank.views import CallResponseView


def run() -> None:
    """
    Entry point of the application.
    """
    app = web.Application()

    app["config"] = config

    app.on_startup.append(init_db)

    app.on_cleanup.append(close_db)

    app.router.add_view("/process_call", CallResponseView)

    graphql_handlers = register_graphql_handlers(
        app=app,
        engine_sdl=[
            os.path.join(config.ROOT, "bank/sdl/Query.graphql"),
            os.path.join(config.ROOT, "bank/sdl/Mutation.graphql"),
            os.path.join(config.ROOT, "bank/sdl/Subscription.graphql"),
        ],
        engine_modules=[
            "bank.resolvers.mutation",
        ],
        executor_http_endpoint="/graphql",
        executor_http_methods=["POST"],
        graphiql_enabled=True,
        subscription_ws_endpoint="/ws",
    )

    web.run_app(graphql_handlers)
