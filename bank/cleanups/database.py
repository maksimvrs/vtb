from aiohttp import web


async def close_db(app: web.Application):
    await app['db'].close()
