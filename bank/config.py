import os

from typing import Optional
from decimal import Decimal

from environs import Env
from marshmallow.validate import OneOf

env = Env()
env.read_env()

DEBUG: bool = env.bool("DEBUG", True)
HOST: str = env("HOST", "localhost")
PORT: int = env("PORT", 80)
ROOT: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
ENVIRONMENT: str = env(
    "ENVIRONMENT",
    "development",
    validate=OneOf(
        ["production", "staging", "development"],
        error="ENVIRONMENT must be one of: {choices}",
    ),
)
LOGGER_FORMAT: str = env.str(
    "LOGGER_FORMAT", "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
DATABASE_URI: str = (
    env("DATABASE_URI", "")
    or "{driver}://{user}:{password}@{host}:{port}/{db}".format(
        driver=env("DATABASE_DRIVER"),
        user=env("DATABASE_USER"),
        password=env("DATABASE_PASSWORD"),
        host=env("DATABASE_HOST"),
        port=env("DATABASE_PORT", 5432),
        db=env("DATABASE_NAME"),
    )
)
DATABASE_POOL_MIN_SIZE: int = env.int("DATABASE_POOL_MIN_SIZE", 1)
DATABASE_POOL_MAX_SIZE: int = env.int("DATABASE_POOL_MAX_SIZE", 16)
DATABASE_ECHO: bool = env.bool("DATABASE_ECHO", DEBUG)
DATABASE_SSL: Optional[str] = env("DATABASE_SSL", None)
DATABASE_USE_CONNECTION_FOR_REQUEST: bool = env.bool(
    "DATABASE_USE_CONNECTION_FOR_REQUEST", True
)
DATABASE_RETRY_LIMIT: int = env.int("DATABASE_RETRY_LIMIT", 1)
DATABASE_RETRY_INTERVAL: int = env.int("DATABASE_RETRY_INTERVAL", 1)

FRAUD_SESSION_RATE: int = env.int("FRAUD_SESSION_RATE", 5)

GEO_IP_DATA_PATH: str = env("GEO_IP_DATA_PATH", )
GEO_IP_DB_NAME: str = env("GEO_IP_DB_NAME", "GeoLite2-City.mmdb")

TWILIO_ACCOUNT_SID: str = env("TWILIO_ACCOUNT_SID", )
TWILIO_AUTH_TOKEN: str = env("TWILIO_AUTH_TOKEN", )
TWILIO_FROM_PHONE: str = env("TWILIO_FROM_PHONE", )
