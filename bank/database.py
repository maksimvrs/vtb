from gino import Gino

from bank import config

db = Gino(
    config.DATABASE_URI,
    # pool_min_size=config.DATABASE_POOL_MIN_SIZE,
    # pool_max_size=config.DATABASE_POOL_MAX_SIZE,
    # echo=config.DATABASE_ECHO,
    # ssl=config.DATABASE_SSL,
    # use_connection_for_request=config.DATABASE_USE_CONNECTION_FOR_REQUEST,
    # retry_limit=config.DATABASE_RETRY_LIMIT,
    # retry_interval=config.DATABASE_RETRY_INTERVAL,
)
