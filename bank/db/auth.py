from datetime import datetime

from bank import config
from bank.database import db


class User(db.Model):
    PHONE_SIZE = 15
    PASSWORD_HASH_SIZE = 60

    __tablename__ = "users"

    id = db.Column(db.BigInteger(), primary_key=True)
    phone = db.Column(db.String(PHONE_SIZE), unique=True, nullable=False)
    password_hash = db.Column(db.String(PASSWORD_HASH_SIZE), nullable=False)


class UserSessions(db.Model):
    __tablename__ = "user_sessions"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer,
        db.ForeignKey(User.id, ondelete="SET NULL", onupdate="CASCADE"),
        nullable=True
    )
    started_at = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow
    )
    ip = db.Column(db.String(45), nullable=True)
    city = db.Column(db.String(128), nullable=True)
    country = db.Column(db.String(128), nullable=True)
    browser = db.Column(db.String(128), nullable=True)
    os = db.Column(db.String(128), nullable=True)
    device = db.Column(db.String(128), nullable=True)
    timezone = db.Column(db.String(128), nullable=True)
    displayed_user_agent = db.Column(db.String(128), nullable=True)
    raw_user_agent = db.Column(db.String(), nullable=True)
    is_anonymous_proxy = db.Column(db.Boolean(), nullable=False, default=False)
