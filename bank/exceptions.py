from typing import Optional
from tartiflette import TartifletteError

from bank.errors import ErrorCodes


class DomainException(TartifletteError):
    def __init__(self, code: ErrorCodes, message: Optional[str] = None):
        super().__init__(message)
        self.extensions = {
            "code": code.value,
            "message": message,
        }
