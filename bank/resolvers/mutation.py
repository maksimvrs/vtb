from typing import Any, Dict, Optional

from tartiflette import Resolver

from bank.schemas import (SigninInput, SigninInputSchema, UserSchema)
from bank.services import UserService


@Resolver("Mutation.signin")
async def resolve_mutation_signin(
    parent: Optional[Any],
    args: Dict[str, Any],
    ctx: Dict[str, Any],
    info: "ResolveInfo",
) -> Dict[str, Any]:
    """
    Resolver in charge of the mutation of a recipe.
    :param parent: initial value filled in to the engine `execute` method
    :param args: computed arguments related to the mutation
    :param ctx: context filled in at engine initialization
    :param info: information related to the execution and field resolution
    :type parent: Optional[Any]
    :type args: Dict[str, Any]
    :type ctx: Dict[str, Any]
    :type info: ResolveInfo
    :return: the mutated recipe
    :rtype: Dict[str, Any]
    :raises Exception: if the recipe id doesn't exist
    """
    signin_input = SigninInputSchema().load(args["input"])
    user_service = UserService(request=ctx["req"])
    user: User = await user_service.signin(signin_input)
    return UserSchema().dump(user)
