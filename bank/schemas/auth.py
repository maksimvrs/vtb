from dataclasses import dataclass, field
from typing import List, Optional

import phonenumbers
import marshmallow_dataclass
import marshmallow.validate
from marshmallow import fields, ValidationError
from marshmallow_dataclass import NewType


class PhoneField(fields.Field):
    """Phone number field.
    """
    def _serialize(self, value, attr, obj, **kwargs):
        return value

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            phone_number_object = phonenumbers.parse(value, None)
            return phonenumbers.format_number(
                phone_number_object, phonenumbers.PhoneNumberFormat.E164
            )
        except phonenumbers.phonenumberutil.NumberParseException as error:
            raise ValidationError("Invalid phone number format.") from error


Phone = NewType("Phone", str, field=PhoneField)


@dataclass
class User:
    id: int = field(metadata=dict(dump_only=True))
    phone: Phone = field()
    password: str = field(metadata=dict(load_only=True))


@dataclass
class SigninInput:
    phone: Phone = field()
    password: str = field(metadata=dict(load_only=True))


UserSchema = marshmallow_dataclass.class_schema(User)
SigninInputSchema = marshmallow_dataclass.class_schema(SigninInput)
