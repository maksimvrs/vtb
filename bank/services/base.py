from aiohttp.web import Request


class BaseService:
    def __init__(self, request: Request):
        self.request = request
