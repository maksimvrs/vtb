from sqlalchemy import and_, or_

from bank.db import UserSessions as UserSessionsModel
from bank import config

from .base import BaseService


class SessionsService(BaseService):
    async def is_suspicious(
        self, user_id: int, session: UserSessionsModel
    ) -> bool:
        fraud_rate: float = await self.get_fraud_rate(
            user_id=user_id, session=session
        )
        return fraud_rate >= config.FRAUD_SESSION_RATE

    async def get_fraud_rate(
        self, user_id: int, session: UserSessionsModel
    ) -> float:
        sessions = await UserSessionsModel.query.where(
            and_(
                UserSessionsModel.user_id == user_id,
                or_(
                    and_(
                        UserSessionsModel.city == session.city,
                        UserSessionsModel.country == session.country,
                        UserSessionsModel.city.isnot(None),
                        UserSessionsModel.country.isnot(None)
                    ),
                    and_(
                        UserSessionsModel.ip == session.ip,
                        UserSessionsModel.ip.isnot(None)
                    )
                )
            )
        ).gino.all()
        # Oh man what are you doing here...
        if len(sessions) > 1:
            return 0
        else:
            return 7
