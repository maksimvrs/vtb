from typing import Optional
from abc import ABC, abstractmethod, abstractproperty

from aiohttp.web import Request


class BaseUserSessionsProcessor(ABC):
    def __init__(self, request: Request):
        self.request: Request = request

    @abstractmethod
    async def process(self) -> None:
        raise NotImplementedError

    @property
    @abstractproperty
    def ip(self) -> Optional[str]:
        raise NotImplementedError

    @property
    @abstractproperty
    def city(self) -> Optional[str]:
        raise NotImplementedError

    @property
    @abstractproperty
    def country(self) -> Optional[str]:
        raise NotImplementedError

    @property
    @abstractproperty
    def browser(self) -> Optional[str]:
        raise NotImplementedError

    @property
    @abstractproperty
    def os(self) -> Optional[str]:
        raise NotImplementedError

    @property
    @abstractproperty
    def device(self) -> Optional[str]:
        raise NotImplementedError

    @property
    @abstractproperty
    def timezone(self) -> Optional[str]:
        raise NotImplementedError

    @property
    @abstractproperty
    def displayed_user_agent(self) -> Optional[str]:
        raise NotImplementedError

    @property
    @abstractproperty
    def raw_user_agent(self) -> str:
        raise NotImplementedError

    @property
    @abstractproperty
    def is_anonymous_proxy(self) -> bool:
        raise NotImplementedError
