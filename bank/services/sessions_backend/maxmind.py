import logging
from typing import Optional
from os import path

import geoip2.database
from user_agents import parse

from bank import config

from .base import BaseUserSessionsProcessor


class MaxmindUserSessionsProcessor(BaseUserSessionsProcessor):
    def __init__(self, request):
        super().__init__(request)
        self._city: Optional[str] = None
        self._country: Optional[str] = None
        self._is_anonymous_proxy: bool = False
        self._user_agent = None

    async def process(self):
        try:
            self._user_agent = parse(self.raw_user_agent)
            geoip_database_path = path.join(
                config.GEO_IP_DATA_PATH, config.GEO_IP_DB_NAME
            )
            with geoip2.database.Reader(geoip_database_path) as reader:
                response = reader.city(self.ip)
                self._city = response.city.name
                self._country = response.country.name
            self._is_anonymous_proxy = False
        except Exception as error:
            logging.error(error)

    @property
    def ip(self):
        return self.request.headers.get("X-FORWARDED-FOR", None)

    @property
    def city(self):
        return self._city

    @property
    def country(self):
        return self._country

    @property
    def browser(self):
        if self._user_agent is None:
            return None
        return self._user_agent.browser.family

    @property
    def os(self):
        if self._user_agent is None:
            return None
        return self._user_agent.os.family

    @property
    def device(self):
        if self._user_agent is None:
            return None
        return self._user_agent.device.family

    @property
    def timezone(self):
        return None

    @property
    def displayed_user_agent(self):
        return f"{self._user_agent.browser.family} {self._user_agent.os.family}"

    @property
    def raw_user_agent(self):
        return self.request.headers.get("User-Agent", None)

    @property
    def is_anonymous_proxy(self):
        return self._is_anonymous_proxy
