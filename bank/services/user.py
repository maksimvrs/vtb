from typing import Optional
from datetime import datetime

import bcrypt
from sqlalchemy import func

from bank.schemas import User, SigninInput
from bank.db import User as UserModel, UserSessions as UserSessionsModel
from bank.database import db
from bank.exceptions import DomainException
from bank.errors import ErrorCodes
from bank.services.sessions_backend import MaxmindUserSessionsProcessor
from bank.services.verify import VerifyService
from bank.services.sessions import SessionsService

from .base import BaseService


class UserService(BaseService):
    async def get_user(self, phone: str) -> Optional[UserModel]:
        return await UserModel.query.where(UserModel.phone == phone
                                           ).gino.one_or_none()

    @staticmethod
    def hash_password(password: str):
        return bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()

    async def signup(self, data: SigninInput) -> UserModel:
        user = UserModel(
            phone=data.phone,
            password_hash=UserService.hash_password(data.password)
        )
        return await user.create()

    @staticmethod
    def check_password(user: UserModel, password: str) -> bool:
        return bcrypt.checkpw(password.encode(), user.password_hash.encode())

    async def signin(self, data: SigninInput) -> UserModel:
        user = await self.get_user(phone=data.phone)
        if user is None:
            user = await self.signup(data)
        else:
            if not self.check_password(user, data.password):
                raise DomainException(code=ErrorCodes.WRONG_PASSWORD)
        # TODO: Transaction
        session = await self.start_session(user_id=user.id)

        session_service = SessionsService(request=self.request)
        verify_service = VerifyService(request=self.request)

        is_suspicious = await session_service.is_suspicious(
            user_id=user.id, session=session
        )
        if is_suspicious:
            await verify_service.ask_call_come(user=user)
        else:
            await verify_service.send_code(phone=user.phone)
        return user

    async def start_session(self, user_id: int) -> UserSessionsModel:
        session_processor = MaxmindUserSessionsProcessor(self.request)
        await session_processor.process()
        session = UserSessionsModel(
            user_id=user_id,
            ip=session_processor.ip,
            started_at=datetime.utcnow(),
            city=session_processor.city,
            country=session_processor.country,
            browser=session_processor.browser,
            os=session_processor.os,
            device=session_processor.device,
            timezone=session_processor.timezone,
            displayed_user_agent=session_processor.displayed_user_agent,
            raw_user_agent=session_processor.raw_user_agent,
            is_anonymous_proxy=session_processor.is_anonymous_proxy,
        )
        return await session.create()
