import random
from enum import Enum

from twilio.rest import Client
from twilio.twiml.voice_response import VoiceResponse, Say, Gather

from bank.db import User as UserModel
from bank import config

from .base import BaseService


class CallResponseEnum(Enum):
    CALLED = 1
    NOT_CALLED = 2


class VerifyService(BaseService):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = Client(
            config.TWILIO_ACCOUNT_SID, config.TWILIO_AUTH_TOKEN
        )
        self.random = random.SystemRandom()

    async def send_code(self, phone: str) -> None:
        code = self.random.randrange(1000, 999999)
        message = self.client.messages.create(
            body=f"Код подтверждения: {code}",
            from_=config.TWILIO_FROM_PHONE,
            to=phone
        )

    async def ask_call_come(self, user: UserModel) -> None:
        text = (
            "Добрый день. Это бот банка ВТБ. "
            "Мы зафиксировали подозрительную попытку входа в ваш личный "
            "кабинет с нового устройства."
        )
        gather_text = (
            "В случае если сегодня с "
            "вами связывались представители нашего банка, нажмите "
            f"“{CallResponseEnum.CALLED.value}”. В противном случае "
            f" нажмите “{CallResponseEnum.NOT_CALLED.value}”. После этого вам "
            "будет выслан код подтверждения."
        )
        error_text = "Я вас не поняла, пожалуйста, давайте попробуем еще раз."

        callback_url = f"{config.HOST}/process_call"

        voice = VoiceResponse()
        voice.say(text, language='ru-RU')

        with voice.gather(
            action=callback_url,
            finish_on_key='#',
            language='ru-RU',
            method='POST',
            numDigits='1',
            timeout='40'
        ) as gather:
            gather.say(gather_text, language='ru-RU')

        voice.say(error_text, language='ru-RU')
        voice.hangup()

        call = self.client.calls.create(
            twiml=voice, to=user.phone, from_=config.TWILIO_FROM_PHONE
        )

    async def process_call_response(self, phone: str, number: int):
        voice = VoiceResponse()
        if number == CallResponseEnum.CALLED.value:
            text = (
                "Мы проверили информацию. Наши сотрудники с вами не "
                "связывались. Вам звонили мошенники. В целях безопасности "
                "ваш счет был заморожен. Пожалуйста, оставайтесь на линии, наш "
                "сотрудник расскажет о дальнейших действиях."
            )
            voice.say(text, language='ru-RU')
        elif number == CallResponseEnum.NOT_CALLED.value:
            await self.send_code(phone=phone)
            voice.say(
                "Спасибо за ответ! Код подтверждения выслан. До свидания!",
                language='ru-RU'
            )
        else:
            voice.say(
                "Я вас не поняла, пожалуйста, давайте попробуем еще раз.",
                language='ru-RU'
            )
        voice.hangup()
        return str(voice)