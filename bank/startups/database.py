from aiohttp import web

from bank.database import db


async def init_db(app: web.Application) -> None:
    app['db'] = await db.set_bind(app["config"].DATABASE_URI)
