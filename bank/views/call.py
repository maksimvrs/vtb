from aiohttp import web

from bank import config
from bank.services import VerifyService


class CallResponseView(web.View):
    async def post(self):
        data = await self.request.post()
        verify = VerifyService(request=self.request)
        responce = await verify.process_call_response(
            phone=data["Called"],
            number=int(data["Digits"]),
        )
        return web.Response(text=responce, content_type='text/xml', status=200)
